# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from cms.apphook_pool import apphook_pool

from shop.cms_apphooks import CatalogListCMSApp, CatalogSearchCMSApp


class CatalogListApp(CatalogListCMSApp):
    def get_urls(self, page=None, language=None, **kwargs):
        from shop.search.views import CMSPageCatalogWrapper
        from shop.views.catalog import AddToCartView, ProductRetrieveView
        from .filters import ManufacturerFilterSet
        from .serializers import CatalogSearchSerializer

        return [
            url(r'^$', CMSPageCatalogWrapper.as_view(
                filter_class=ManufacturerFilterSet,
                search_serializer_class=CatalogSearchSerializer,
            )),
            url(r'^(?P<slug>[\w-]+)/?$', ProductRetrieveView.as_view(
                use_modal_dialog=False,
            )),
            url(r'^(?P<slug>[\w-]+)/add-to-cart', AddToCartView.as_view()),
        ]


apphook_pool.register(CatalogListApp)


class CatalogSearchApp(CatalogSearchCMSApp):
    def get_urls(self, page=None, language=None, **kwargs):
        from shop.search.views import SearchView
        from .serializers import ProductSearchSerializer

        return [
            url(r'^', SearchView.as_view(
                serializer_class=ProductSearchSerializer,
            )),
        ]


apphook_pool.register(CatalogSearchApp)
